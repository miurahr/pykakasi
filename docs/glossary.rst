.. _glossary:

Glossary
========

.. glossary::

   wakati gaki
      separating a sentence into words -- ordinary japanese text does not use
      space for separating words, instead readers are expected to use heuristics
      to understand do so themselves.

   Romaji
      alphabetical description of Japanese pronunciation.
